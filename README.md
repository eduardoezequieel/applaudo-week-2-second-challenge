# applaudo-first-challenge

<img src="img/1.png" alt="landing-page">
<img src="img/2.png" alt="github">
<img src="img/3.png" alt="github">

## About this repository

Application developed with GitHub API, shows the data of the searched user, extracted from his/her profile. Just run it in your browser, some data is saved on LocalStorage (Color mode values)
