export default class Validator {
  validateUsername(id, error) {
    const input = document.getElementById(id);
    if (input.value.match(/^[a-z\d](?:[a-z\d]|-(?=[a-z\d])){0,38}$/i)) {
      input.style.border = '1px solid green';

      const errorLabel = document.querySelector(`#${input.id}Error`);
      if (errorLabel !== null) {
        errorLabel.remove();
      }
    } else {
      input.style.border = '1px solid red';

      if (document.querySelector(`#${input.id}Error`) === null) {
        const errorLabel = document.createElement('label');
        errorLabel.textContent = error;
        errorLabel.className = 'ms-5 error';
        errorLabel.id = `${input.id}Error`;
        input.parentNode.parentNode.appendChild(errorLabel);
      }
    }
  }

  clearForms() {
    const forms = document.querySelectorAll('form');
    const inputs = document.querySelectorAll('input');
    const errors = document.querySelectorAll('label.error');

    forms.forEach((element) => {
      element.reset();
    });

    inputs.forEach((element) => {
      element.style.border = '1px solid transparent';
    });

    errors.forEach((element) => {
      element.remove();
    });
  }

  showHiddenLabels() {
    const hide = document.querySelectorAll('p.d-none, a.d-none');

    hide.forEach((element) => {
      element.classList.remove('d-none');
    });
  }
}
