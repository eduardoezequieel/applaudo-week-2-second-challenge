export default class Fetcher {
  API_GITHUB = 'https://api.github.com';

  async getUserData(user) {
    const request = await fetch(`${this.API_GITHUB}/users/${user}`, {
      method: 'GET',
      headers: {
        authorization: 'token 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',
      },
    });

    if (request.ok) {
      return await request.json();
    } else {
      if (request.status == 404) {
        return {
          error: true,
          message: 'User not found',
        };
      } else {
        return {
          error: true,
          message: 'Something wrong happened',
        };
      }
    }
  }

  async getRepositories(user) {
    const request = await fetch(`${this.API_GITHUB}/users/${user}/repos`, {
      method: 'GET',
      headers: {
        authorization: 'token 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',
      },
    });

    if (request.ok) {
      return await request.json();
    } else {
      if (request.status == 404) {
        return {
          error: true,
          message: 'User repositories not found',
        };
      } else {
        return {
          error: true,
          message: 'Something wrong happened',
        };
      }
    }
  }
}
