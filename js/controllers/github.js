import GitHubUI from '../views/github-ui.js';
import Validator from '../helpers/validator.js';

const githubManagement = new GitHubUI();
const validator = new Validator();

document.addEventListener('DOMContentLoaded', () => {
  validator.clearForms();
});

document.querySelector('#searchUser-form').addEventListener('submit', (e) => {
  e.preventDefault();
  githubManagement.setUserData(document.getElementById('txtUsername').value, 'submit');
});

document.querySelectorAll("input[type='text'], input[maxlength='39']").forEach((element) => {
  element.addEventListener('input', (e) => {
    const id = e.currentTarget.id;
    const message = e.currentTarget.getAttribute('message');
    validator.validateUsername(id, message);
  });
});

let timer;
document.querySelector('#txtUsername').addEventListener('keyup', (e) => {
  if (e.keyCode != 13 && e.keyCode != 16 && e.keyCode != 9) {
    clearTimeout(timer);
    timer = setTimeout(() => {
      githubManagement.setUserData(e.target.value, 'on-search');
    }, 500);
  }
});
