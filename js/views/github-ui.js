import Fetcher from '../helpers/fetcher.js';
import Validator from '../helpers/validator.js';
export default class GitHubUI {
  fetcher = new Fetcher();
  validator = new Validator();

  setUserData(user, type) {
    if (document.querySelector('.error') == null) {
      this.fetcher.getUserData(user).then((response) => {
        if (!response.error) {
          document.querySelector('#data-container').className = 'row';
          document.querySelector('#greetings-message').className = 'd-none';

          document.getElementById('picGithub').setAttribute('src', response.avatar_url);
          document.getElementById('lblName').textContent = response.name;
          document.getElementById('lblUsername').textContent = `@${response.login}`;
          document.getElementById('lblMemberSince').textContent = response.created_at.substring(0, 4);
          document.getElementById('githubLink').setAttribute('href', response.html_url);
          document.getElementById('lblFollowers').textContent = new Intl.NumberFormat().format(response.followers);
          document.getElementById('lblFollowing').textContent = new Intl.NumberFormat().format(response.following);
          document.getElementById('lblRepositories').textContent = new Intl.NumberFormat().format(response.public_repos);

          this.validator.showHiddenLabels();

          response.company != null
            ? (document.getElementById('lblCompany').textContent = response.company)
            : document.getElementById('lblCompany').parentElement.classList.add('d-none');

          response.location != null
            ? (document.getElementById('lblLocation').textContent = response.location)
            : document.getElementById('lblLocation').parentElement.classList.add('d-none');

          document.getElementById('blogLink').parentElement.setAttribute('href', response.blog);
          response.blog != ''
            ? (document.getElementById('blogLink').textContent = response.blog)
            : document.getElementById('blogLink').parentElement.classList.add('d-none');

          this.setRepositories(user);
          document.querySelector('#info-container').className = '';
          document.querySelector('#info-container').className = 'animate__animated animate__fadeInUp';
        } else {
          if (type == 'submit') {
            Swal.fire('Warning', response.message, 'warning');
          }
        }
      });
    } else {
      if (type == 'submit') {
        Swal.fire('Warning', 'Invalid username', 'warning');
      }
    }
  }

  setRepositories(user) {
    this.fetcher.getRepositories(user).then((response) => {
      if (!response.error) {
        let rows = '';
        let stars = 0;
        response.map((row) => {
          stars = parseInt(stars) + parseInt(row.stargazers_count);
          rows += `
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-4 animate__animated animate__fadeInUp">
                <div class="repository-block d-flex flex-column justify-content-between">
                <div>
                    <a href="${row.clone_url}" class="m-0 custom-text-normal">${row.name}</a>
                    <p class="m-0 custom-text-mutted-2">${row.description != null ? row.description : 'No description.'}</p>
                </div>
                <div class="d-flex">
                    <p class="m-0 custom-text-mutted me-4"><i class="bi bi-eye-fill me-1"></i>${new Intl.NumberFormat().format(row.watchers)}</p>
                    <p class="m-0 custom-text-mutted me-4"><i class="bi bi-diagram-2-fill me-1"></i>${new Intl.NumberFormat().format(row.forks)}</p>
                    <p class="m-0 custom-text-mutted me-4"><i class="bi bi-star-fill me-1"></i>${new Intl.NumberFormat().format(
                      row.stargazers_count
                    )}</p>
                </div>
                </div>
            </div>
            `;
        });

        document.getElementById('lblStars').textContent = new Intl.NumberFormat().format(stars);
        document.getElementById('repo-table').innerHTML = rows;
      } else {
        Swal.fire('Warning', response.message, 'warning');
      }
    });
  }
}
